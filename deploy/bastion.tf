data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["sg-0ecf2dd2e1b63edc2"]
  subnet_id              = "subnet-0a81d4adfe1dfdbc5"

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}